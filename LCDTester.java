import java.util.Scanner;

public class LCDTester {

	static final String CADENA_FINAL = "0,0";

	public static void main(String[] args) {
		try {
			Scanner lector = new Scanner(System.in);

			System.out.print("Espacio entre Digitos (0 a 5): ");
			String comando = lector.next();

			int espacioDig = Integer.parseInt(comando);

			// Se valida que el espaciado este entre 0 y 5
			if (espacioDig < 0 || espacioDig > 5) {
				lector.close();
				throw new IllegalArgumentException("El espacio entre digitos debe estar entre 0 y 5");
			}
			
			ImpresorLCD impresorLCD = new ImpresorLCD();
			
			System.out.print("Entrada: ");
			comando = lector.next();
			
			while (!comando.equalsIgnoreCase(CADENA_FINAL)) {
				impresorLCD.procesar(comando, espacioDig);
				System.out.print("Entrada: ");
				comando = lector.next();	
			}
			
			lector.close();
		}catch(NumberFormatException ex) {
			System.out.println("El espacio entre digitos no es un entero");	
		}catch (IllegalArgumentException ex) {
			System.out.println(ex.getMessage());
		}catch (Exception ex) {
			System.out.println("Error: " + ex.getMessage());
		}
	}

}
